const mongoose = require("mongoose"),
    Schema = mongoose.Schema,
    model = mongoose.model

const EditorSchema = new Schema({
    title: {
        type: String,
        required: [true, "nhập title"]
    },
    content: {
        type: String,
        required: [true, "nhập content"]
    }
})

module.exports = model("editor", EditorSchema)