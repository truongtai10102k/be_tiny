const EditorModel = require("../model/index")

exports.getEditor = async (req, res) => {
    try {
        let result = await EditorModel.find({});
        res.json(result)
    } catch (error) {
        res.json({ message: error.message })
    }
}

exports.postEditor = async (req, res) => {
    try {
        let { title, content } = req.body;
        let newFile = new EditorModel({
            title: title,
            content: content
        })
        const file = await newFile.save();

        res.json(file)
    } catch (error) {
        res.json({ message: error.message })
    }
}

exports.putEditor = async (req, res) => {
    try {
        let _id = req.params.id;
        let newFile = {
            title: req.body.title,
            content: req.body.content
        }
        let result = await EditorModel.findByIdAndUpdate(_id, newFile, { new: true });

        res.json(result)
    } catch (error) {
        res.json({ message: error.message })
    }
}

exports.deleteEditor = async (req, res) => {
    try {
        let _id = req.params.id;

        let result = await EditorModel.findById(_id);
        await result.delete()

        res.json({ message: "delete success" })
    } catch (error) {
        res.json({ message: error.message })
    }
}

exports.uploadEditor = (req, res) => {
    res.json({ location: "http://localhost:3001/images/" + req.files[0].filename })
}