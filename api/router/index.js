const express = require("express");
const EditorRouter = require("./editorRouter")
let Router = express.Router();
Router.use("/editor", EditorRouter)

module.exports = Router;