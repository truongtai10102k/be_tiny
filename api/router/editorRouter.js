const express = require("express");
let Router = express.Router();
const EditorContainer = require("../container/editorController")
Router.route("/")
    .get(EditorContainer.getEditor)
    .post(EditorContainer.postEditor)
Router.route("/:id")
    .put(EditorContainer.putEditor)
    .delete(EditorContainer.deleteEditor)
Router.route("/upload-editor")
    .post(EditorContainer.uploadEditor)

module.exports = Router;