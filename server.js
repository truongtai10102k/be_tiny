const express = require('express');
const app = express();
const logger = require("morgan");
const mongoose = require("mongoose");
const cors = require("cors");
const multer = require("multer");
const path = require("path");

//bắt dữ liệu json
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(logger("dev"));
// public folder 
app.use(express.static("public"))
// connect database


mongoose.connect("mongodb://localhost:27017/uploadfile",
    { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
        if (err) console.log("connect to database error : ", err);
        else {
            console.log("connect database success !!");
        }
    })
const PORT = 3001;

app.use("/home", (req, res) => {
    res.send("this a hompage")
})

let diskStorage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null,path.join(__dirname, "public/images"));
    },
    filename: (req, file, callback) => {
        console.log(file.originalname);
        callback(null, file.originalname);
    },
});

app.use(multer({ storage: diskStorage }).any())

// call router
const Router = require("./api/router/index")
app.use("/api", Router)


app.use((req, res) => {
    res.send("url " + req.originalUrl + " not found!!")
})

app.listen(PORT, (err) => {
    if (err) {
        console.log("server run error ", err);
    } else {
        console.log("server running on " + PORT);
    }
})

